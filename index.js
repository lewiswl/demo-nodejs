const http = require('http');
const fs = require('fs');

let httpServer = http.createServer((req, res) => {
	let url = req.url.split('?')[0];
	if (url === '/') {
		url = '/index.html';
	}
	let fstream = fs.createReadStream(__dirname + url);
	fstream.on('open', () => {
		res.writeHead(200);
		fstream.pipe(res);
	});
	fstream.on('error', er => {
		console.log(er);
		res.writeHead(404);
		res.end(`<h1>Page '${url}' not found</h1>`);
	});
})
httpServer.on('error', er => {
	console.log(er);
});
httpServer.listen(80);